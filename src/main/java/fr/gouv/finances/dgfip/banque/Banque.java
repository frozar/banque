package fr.gouv.finances.dgfip.banque;

public class Banque {

  protected String codeBanque;

  public Banque(String codeBanque) {
    this.codeBanque = codeBanque;
  }

  @Override
  public String toString() {
    return "Banque [codeBanque=" + codeBanque + "]";
  }

}
